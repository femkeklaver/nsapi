import math
import statistics
from collections import defaultdict
from pprint import pprint

import confidence

import ns


def iterate_travel_options(from_stations,
                           to_stations,
                           traveltime,
                           way_back=False):
    min_travel_dict = defaultdict(list)

    for to_station in to_stations:
        for from_station in from_stations:
            station = to_station if not way_back else from_station
            response = ns.get_travelinfo(from_station, to_station, traveltime,
                                         auth)
            traveltimes = ns.parse_travelinfo(response)
            travel_transfers = [
                planned_time * (math.log10(transfers + 1) + 1)
                for planned_time, transfers in zip(*traveltimes)
            ]
            min_travel_dict[station].append(min(travel_transfers, default=0))
    return min_travel_dict


if __name__ == "__main__":
    conf = confidence.loadf('nsapi.yaml')
    auth = (conf.login.user, conf.login.token)
    tijd_heen = conf.travelinfo.departure_heen
    tijd_terug = conf.travelinfo.departure_terug

    woonplaatsen = conf.travelinfo.woonplaatsen
    bestemmingen = conf.travelinfo.bestemmingen

    travel_dict_heen = iterate_travel_options(woonplaatsen, bestemmingen,
                                              tijd_heen)

    print("Max tijd heen")
    pprint([(station, max(travel))
            for station, travel in travel_dict_heen.items()])
    print("Avg tijd heen")
    pprint([(station, statistics.mean(filter(lambda x: x > 0, travel)))
            for station, travel in travel_dict_heen.items()])

    print("Max tijd terug")
    travel_dict_terug = iterate_travel_options(
        bestemmingen, woonplaatsen, tijd_terug, way_back=True)
    pprint([(station, max(travel))
            for station, travel in travel_dict_terug.items()])
    print("Avg tijd terug")
    pprint([(station, statistics.mean(filter(lambda x: x > 0, travel)))
            for station, travel in travel_dict_terug.items()])

    # pprint(travel_dict_terug)