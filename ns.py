from lxml import etree
import requests


def get_travelinfo(from_station, to_station, traveltime, auth):
    params = {
        'fromStation': from_station,
        'toStation': to_station,
        'dateTime': traveltime,
        'previousAdvices': 2,
        'nextAdvices': 2
    }
    response = requests.get(
        'http://webservices.ns.nl/ns-api-treinplanner', params, auth=auth)
    return response


def parse_time(timestamp):
    hours, minutes = timestamp.split(":")
    return (int(hours) * 60) + int(minutes)


def parse_travelinfo(response):
    """return expected traveltime + number of transfers"""
    tree = etree.fromstring(response.text.encode('utf-8'))
    traveltimes = [
        parse_time(elem.text)
        for elem in tree.xpath('ReisMogelijkheid/GeplandeReisTijd')
    ]
    n_transfers = [
        int(elem.text)
        for elem in tree.xpath('ReisMogelijkheid/AantalOverstappen')
    ]
    return traveltimes, n_transfers
